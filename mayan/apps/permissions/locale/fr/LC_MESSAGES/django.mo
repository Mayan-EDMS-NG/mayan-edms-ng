��             +         �     �     �  O   �  F   @     �     �     �     �     �  
   �     �     �     �  
   �     �               3     9  	   >     H  
   _     j     v     �     �     �     �     �     �  
   �  �  �     a     u  k   �  \   �     V     n  	   �     �     �     �  	   �     �     �     �     �          !  
   5     @     D  #   V     z     �     �     �     �     �     �     �     �     �                                                                                                               
                          	       Available groups Available permissions Comma separated list of groups primary keys to add to, or replace in this role. Comma separated list of permission primary keys to grant to this role. Create new role Create roles Delete Delete roles Edit Edit roles Grant Grant permissions Granted permissions Group name Groups Groups of role: %s Insufficient permissions. Label Name Namespace No such permission: %s Permission Permissions Permissions for role: %s Revoke Revoke permissions Role Role groups Role permissions Roles View roles Project-Id-Version: Mayan EDMS
Report-Msgid-Bugs-To: 
PO-Revision-Date: 2017-08-07 12:43+0000
Last-Translator: Christophe CHAUVET <christophe.chauvet@gmail.com>
Language-Team: French (http://www.transifex.com/rosarior/mayan-edms/language/fr/)
Language: fr
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Plural-Forms: nplurals=2; plural=(n > 1);
 Groupes disponibles Permissions disponibles La liste séparée par une virgule des groupes de clés primaires à ajouter ou à remplacer dans ce rôle. Liste séparée par des virgules des clés primaires d'autorisation pour autoriser ce rôle. Créer un nouveau rôle Créer des rôles Supprimer Supprimer des rôles Modifier Modifier les rôles Attribuer Donner des droits Permissions accordées Nom du groupe Groupes Groupes ayant le rôle : %s Droits insuffisants Étiquette Nom Espace de nommage Aucune autorisation de ce genre: %s Autorisation Droits Permissions pour le rôle : %s Retirer Retirer des droits Rôle Rôles Autorisations du rôle Rôles Afficher les rôles 