# SOME DESCRIPTIVE TITLE.
# Copyright (C) YEAR THE PACKAGE'S COPYRIGHT HOLDER
# This file is distributed under the same license as the PACKAGE package.
#
# Translators:
msgid ""
msgstr ""
"Project-Id-Version: Mayan EDMS\n"
"Report-Msgid-Bugs-To: \n"
"POT-Creation-Date: 2018-03-20 02:05-0400\n"
"PO-Revision-Date: 2016-04-17 09:39+0000\n"
"Last-Translator: Roberto Rosario\n"
"Language-Team: Romanian (Romania) (http://www.transifex.com/rosarior/mayan-"
"edms/language/ro_RO/)\n"
"Language: ro_RO\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=3; plural=(n==1?0:(((n%100>19)||((n%100==0)&&(n!=0)))?"
"2:1));\n"

#: apps.py:30 events.py:8
msgid "Document comments"
msgstr ""

#: apps.py:54
msgid "Date"
msgstr "Data"

#: apps.py:56 models.py:27
msgid "User"
msgstr "utilizator"

#. Translators: Comment here is a noun and refers to the actual text stored
#: apps.py:59 models.py:30 models.py:39
msgid "Comment"
msgstr "Comentariu"

#: apps.py:63 apps.py:67 links.py:22 models.py:40 permissions.py:7
msgid "Comments"
msgstr "Comentarii"

#: events.py:12
msgid "Document comment created"
msgstr ""

#: events.py:15
msgid "Document comment deleted"
msgstr ""

#: links.py:13
msgid "Add comment"
msgstr ""

#: links.py:18
msgid "Delete"
msgstr "Șterge"

#: models.py:23
msgid "Document"
msgstr ""

#: models.py:33
msgid "Date time submitted"
msgstr ""

#: permissions.py:10
msgid "Create new comments"
msgstr "Creați comentarii noi"

#: permissions.py:13
msgid "Delete comments"
msgstr "Ștergeți comentarii"

#: permissions.py:16
msgid "View comments"
msgstr "Vezi comentariile"

#: views.py:40
#, python-format
msgid "Add comment to document: %s"
msgstr "Adaugă comentariu la document:% s"

#: views.py:78
#, python-format
msgid "Delete comment: %s?"
msgstr ""

#: views.py:97
#, python-format
msgid "Comments for document: %s"
msgstr ""
