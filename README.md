[![pypi][pypi]][pypi-url]
[![builds][builds]][builds-url]
[![coverage][cover]][cover-url]
![python][python]
![license][license]

[pypi]: http://img.shields.io/pypi/v/mayan-edms-ng.svg
[pypi-url]: http://badge.fury.io/py/mayan-edms-ng

[builds]: https://gitlab.com/mayan-edms-ng/mayan-edms-ng/badges/master/build.svg
[builds-url]: https://gitlab.com/mayan-edms-ng/mayan-edms-ng/pipelines

[cover]: https://codecov.io/gitlab/mayan-edms/mayan-edms/coverage.svg?branch=master
[cover-url]: https://codecov.io/gitlab/mayan-edms/mayan-edms?branch=master

[python]: https://img.shields.io/pypi/pyversions/mayan-edms-ng.svg
[python-url]: https://img.shields.io/pypi/l/mayan-edms-ng.svg?style=flat

[license]: https://img.shields.io/pypi/l/mayan-edms-ng.svg?style=flat
[license-url]: https://img.shields.io/pypi/l/mayan-edms-ng.svg?style=flat


<div align="center">
  <a href="http://www.mayan-edms.com">
    <img width="200" heigth="200" src="https://gitlab.com/mayan-edms-ng/mayan-edms-ng/raw/master/docs/_static/mayan-pyramid.png">
  </a>
  <br>
  <br>
  <p>
    Mayan EDMS is a document management system. Its main purpose is to store,
    introspect, and categorize files, with a strong emphasis on preserving the
    contextual and business information of documents. It can also OCR, preview,
    label, sign, send, and receive thoses files. Other features of interest
    are its workflow system, role based access control, and REST API.
  <p>

<p align="center">
    <img img width="400" src="https://gitlab.com/mayan-edms-ng/mayan-edms-ng/raw/master/docs/_static/overview.gif">
</p>

</div>

<h2 align="center">Installation</h2>

The easiest way to use Mayan EDMS is by using the official
[Docker](https://www.docker.com/) image. Make sure Docker is properly installed
and working before attempting to install Mayan EDMS.

For the complete set of installation, configuration, upgrade, and backup
instructions visit the Mayan EDMS Docker Hub page at:
https://hub.docker.com/r/mayanedms-ng/mayanedms-ng/

<h2 align="center">Hardware requirements</h2>

- 2 Gigabytes of RAM (1 Gigabyte if OCR is turned off).
- Multiple core CPU (64 bit, faster than 1 GHz recommended).

<h2 align="center">Important links</h2>


- [Homepage](http://www.mayan-edms.com)
- [Videos](https://www.youtube.com/channel/UCJOOXHP1MJ9lVA7d8ZTlHPw)
- [Documentation](http://mayan.readthedocs.io/en/stable/)
- [Paid support](http://www.mayan-edms.com/providers/)
- [Roadmap](https://gitlab.com/mayan-edms-ng/mayan-edms-ng/wikis/roadmap)
- [Contributing](https://gitlab.com/mayan-edms-ng/mayan-edms-ng/blob/master/CONTRIBUTING.md)
- [Community forum](https://groups.google.com/forum/#!forum/mayan-edms)
- [Community forum archive](http://mayan-edms.1003.x6.nabble.com/)
- [Source code, issues, bugs](https://gitlab.com/mayan-edms-ng/mayan-edms-ng)
- [Plug-ins, other related projects](https://gitlab.com/mayan-edms-ng/)
- [Translations](https://www.transifex.com/rosarior/mayan-edms/)

