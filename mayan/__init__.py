from __future__ import unicode_literals

__title__ = 'Mayan EDMS NG'
__version__ = '3.0.2'
__build__ = 0x030002
__author__ = 'Roberto Rosario'
__author_email__ = 'roberto.rosario@mayan-edms.com'
__description__ = 'Free Open Source Electronic Document Management System'
__license__ = 'Apache 2.0'
__copyright__ = 'Copyright 2011-2016 Roberto Rosario'
__website__ = 'https://www.mayan-edms.com'

