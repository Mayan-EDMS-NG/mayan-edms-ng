��    (      \  5   �      p     q     x     �     �     �     �     �     �     �                     7     M     c     s     x     �     �     �     �  )   �  6   �  	   %     /     6     D     W     l     q     ~  [   �  d   �     F     f     �     �     �     �  s  �     9     ?     M     [     o  "   �  	   �     �  	   �     �     �     �     �     	     	  
   0	     ;	     N	     b	     w	     �	  8   �	  P   �	     
     /
     7
     I
     a
     w
     |
     �
  n   �
  n     "   v  #   �     �     �     �     �               #   $                                    "   (                                                      
           '       !          %      	         &                      Active All the groups. All the users. Available groups Available users Change password for user: %s Create new group Create new groups Create new user Create new users Delete Delete existing groups Delete existing users Delete the group: %s? Delete user: %s Edit Edit existing groups Edit existing users Edit group: %s Edit user: %s Email Error deleting user "%(user)s": %(error)s Error reseting password for user "%(user)s": %(error)s Full name Groups Groups joined Groups of user: %s Has usable password? Name Set password Submit Super user and staff user deleting is not allowed, use the admin interface for these cases. Super user and staff user password reseting is not allowed, use the admin interface for these cases. User "%s" created successfully. User "%s" deleted successfully. User management Users View existing groups View existing users Project-Id-Version: Mayan EDMS
Report-Msgid-Bugs-To: 
PO-Revision-Date: 2017-07-15 03:50+0000
Last-Translator: Roberto Rosario
Language-Team: German (Germany) (http://www.transifex.com/rosarior/mayan-edms/language/de_DE/)
Language: de_DE
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Plural-Forms: nplurals=2; plural=(n != 1);
 Aktiv Alle Gruppen. Alle Benutzer Verfügbare Gruppen Verfügbare Nutzer Passwort ändern für Benutzer: %s Erstellen Gruppen anlegen Erstellen Benutzer anlegen Löschen Gruppen löschen Benutzer löschen Gruppe %s löschen? Benutzer löschen: %s Bearbeiten Gruppen bearbeiten Benutzer bearbeiten Gruppe %s bearbeiten Benutzer %s bearbeiten E-Mail Fehler beim Löschen des Benutzers "%(user)s": %(error)s Fehler beim Zurücksetzen des Passworts für den Benutzer  "%(user)s": %(error)s Kompletter Name Gruppen Gruppen vereinigt Gruppen von Benutzer %s Verwendbares Passwort Name Passwort festlegen Absenden Super User und Staff Benutzer löschen ist nicht erlaubt, benutzen Sie die Administratoren-Oberfläche dafür. Super User und Staff Benutzer löschen ist nicht erlaubt, benutzen Sie die Administratoren-Oberfläche dafür. Benutzer "%s" erfolgreich angelegt Benutzer "%s" erfolgreich gelöscht Benutzerverwaltung Benutzer Gruppen anzeigen Benutzer anzeigen 