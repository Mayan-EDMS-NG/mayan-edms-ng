=====
MECRs
=====

Mayan EDMS Request For Comment documents index.

.. contents:: Table of Contents
   :depth: 2
   :local:

Status
======

Draft
-----

.. toctree::
   :maxdepth: 1

   0001-merc-process
   test-writing
   merging-roles-and-groups
   support-forum
   using-javascript-libraries



Type
====

Process
-------
.. toctree::
   :maxdepth: 1

   0001-merc-process

