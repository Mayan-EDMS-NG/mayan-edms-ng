# SOME DESCRIPTIVE TITLE.
# Copyright (C) YEAR THE PACKAGE'S COPYRIGHT HOLDER
# This file is distributed under the same license as the PACKAGE package.
#
# Translators:
# Jesaja Everling <jeverling@gmail.com>, 2017
msgid ""
msgstr ""
"Project-Id-Version: Mayan EDMS\n"
"Report-Msgid-Bugs-To: \n"
"POT-Creation-Date: 2018-03-20 02:06-0400\n"
"PO-Revision-Date: 2017-04-24 21:12+0000\n"
"Last-Translator: Jesaja Everling <jeverling@gmail.com>\n"
"Language-Team: German (Germany) (http://www.transifex.com/rosarior/mayan-"
"edms/language/de_DE/)\n"
"Language: de_DE\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=2; plural=(n != 1);\n"

#: apps.py:19 links.py:8
msgid "REST API"
msgstr "REST API"

#: fields.py:33
#, python-format
msgid "Unable to find serializer class for: %s"
msgstr "Kein Serialisierer gefunden für: %s"

#: links.py:12
#, fuzzy
#| msgid "API Documentation"
msgid "API Documentation (Swagger)"
msgstr "API Dokumentation"

#: links.py:17
#, fuzzy
#| msgid "API Documentation"
msgid "API Documentation (ReDoc)"
msgstr "API Dokumentation"

#: schemas.py:10
#, python-format
msgid "%s API"
msgstr ""
