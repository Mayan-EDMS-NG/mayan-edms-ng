==================================
Mayan EDMS NG v3.0.2 release notes
==================================

Released: March 22, 2018

What's new
==========

- Fix event and document states apps migration depedencies.
- Add the "to=" keyword argument to all ForeignKey, ManayToMany and OneToOne Fields.


Removals
--------
* None


Upgrading from a previous version
---------------------------------


Using PIP
~~~~~~~~~

Type in the console::

    $ pip install mayan-edms-ng==3.0.2

the requirements will also be updated automatically.


Using Git
~~~~~~~~~

If you installed Mayan EDMS by cloning the Git repository issue the commands::

    $ git reset --hard HEAD
    $ git pull

otherwise download the compressed archived and uncompress it overriding the
existing installation.

Next upgrade/add the new requirements::

    $ pip install --upgrade -r requirements.txt


Common steps
~~~~~~~~~~~~

Migrate existing database schema with::

    $ mayan-edms.py performupgrade

Add new static media::

    $ mayan-edms.py collectstatic --noinput

The upgrade procedure is now complete.


Backward incompatible changes
=============================

* None

Bugs fixed or issues closed
===========================

* None

.. _PyPI: https://pypi.python.org/pypi/mayan-edms/
