��          �      L      �     �     �     �     �     �     �                '     A     G  	   L     V  
   m     x     �     �     �  �  �     �     �     �     �  	   �     �     �     �     �          "     (     9     N     Z  	   f     p     u                         
                                                                       	    Available groups Available permissions Delete Edit Grant Grant permissions Granted permissions Groups Insufficient permissions. Label Name Namespace No such permission: %s Permission Permissions Revoke Role Roles Project-Id-Version: Mayan EDMS
Report-Msgid-Bugs-To: 
PO-Revision-Date: 2017-08-03 06:00+0000
Last-Translator: Roberto Rosario
Language-Team: Polish (http://www.transifex.com/rosarior/mayan-edms/language/pl/)
Language: pl
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Plural-Forms: nplurals=4; plural=(n==1 ? 0 : (n%10>=2 && n%10<=4) && (n%100<12 || n%100>14) ? 1 : n!=1 && (n%10>=0 && n%10<=1) || (n%10>=5 && n%10<=9) || (n%100>=12 && n%100<=14) ? 2 : 3);
 Dostępne grupy Dostępne uprawnienia Usunąć Edytuj Przyznać Grant uprawnienia Przyznane uprawnienia Grupy Niewystarczające uprawnienia. Etykieta Nazwa Przestrzeń nazw Brak uprawnienia: %s Uprawnienia Uprawnienia Odwołać Rola Role 