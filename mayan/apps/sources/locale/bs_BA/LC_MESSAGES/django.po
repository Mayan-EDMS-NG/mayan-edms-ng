# SOME DESCRIPTIVE TITLE.
# Copyright (C) YEAR THE PACKAGE'S COPYRIGHT HOLDER
# This file is distributed under the same license as the PACKAGE package.
#
# Translators:
# www.ping.ba <jomer@ping.ba>, 2013
msgid ""
msgstr ""
"Project-Id-Version: Mayan EDMS\n"
"Report-Msgid-Bugs-To: \n"
"POT-Creation-Date: 2018-03-20 02:07-0400\n"
"PO-Revision-Date: 2017-07-10 07:58+0000\n"
"Last-Translator: Roberto Rosario\n"
"Language-Team: Bosnian (Bosnia and Herzegovina) (http://www.transifex.com/"
"rosarior/mayan-edms/language/bs_BA/)\n"
"Language: bs_BA\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=3; plural=(n%10==1 && n%100!=11 ? 0 : n%10>=2 && n"
"%10<=4 && (n%100<10 || n%100>=20) ? 1 : 2);\n"

#: apps.py:40 links.py:35 models.py:69 queues.py:8 settings.py:7 views.py:563
msgid "Sources"
msgstr ""

#: apps.py:55
msgid "Create a document source"
msgstr ""

#: apps.py:57
msgid ""
"Document sources are the way in which new documents are feed to Mayan EDMS, "
"create at least a web form source to be able to upload documents from a "
"browser."
msgstr ""

#: apps.py:67
msgid "Created"
msgstr ""

#: apps.py:74
msgid "Thumbnail"
msgstr ""

#: apps.py:82 models.py:794
msgid "Date time"
msgstr ""

#: apps.py:87 models.py:797
msgid "Message"
msgstr ""

#: forms.py:30
msgid "Comment"
msgstr "Komentar"

#: forms.py:45
msgid "Expand compressed files"
msgstr "Otpakuj kompresovane datoteke"

#: forms.py:47
msgid "Upload a compressed file's contained files as individual documents"
msgstr "Upload kompresovane datoteke koja sadrži individualne dokumente"

#: forms.py:68 views.py:451
msgid "Staging file"
msgstr "Osnovna datoteka"

#: forms.py:72 forms.py:77
msgid "File"
msgstr "Datoteka"

#: handlers.py:16
msgid "Default"
msgstr "default"

#: links.py:30
msgid "New document"
msgstr ""

#: links.py:39
msgid "Add new IMAP email"
msgstr ""

#: links.py:44
msgid "Add new POP3 email"
msgstr ""

#: links.py:49
msgid "Add new staging folder"
msgstr ""

#: links.py:54
msgid "Add new watch folder"
msgstr ""

#: links.py:59
msgid "Add new webform source"
msgstr ""

#: links.py:64
msgid "Add new SANE scanner"
msgstr ""

#: links.py:69 links.py:83
msgid "Delete"
msgstr ""

#: links.py:73
msgid "Edit"
msgstr "Urediti"

#: links.py:77
msgid "Document sources"
msgstr "izvori dokumenata"

#: links.py:89
msgid "Upload new version"
msgstr ""

#: links.py:92
msgid "Logs"
msgstr ""

#: links.py:96
msgid "Check now"
msgstr ""

#: literals.py:9
msgid "Flatbed"
msgstr ""

#: literals.py:10
msgid "Document feeder"
msgstr ""

#: literals.py:17
msgid "Simplex"
msgstr ""

#: literals.py:18
msgid "Duplex"
msgstr ""

#: literals.py:26
msgid "Lineart"
msgstr ""

#: literals.py:27
msgid "Monochrome"
msgstr ""

#: literals.py:28
msgid "Color"
msgstr "Boja"

#: literals.py:36 literals.py:41
msgid "Always"
msgstr "Uvijek"

#: literals.py:37 literals.py:42
msgid "Never"
msgstr "Nikada"

#: literals.py:43
msgid "Ask user"
msgstr "Pitaj korisnika"

#: literals.py:54
msgid "Scanner"
msgstr ""

#: literals.py:55 models.py:389
msgid "Web form"
msgstr ""

#: literals.py:56 models.py:325
msgid "Staging folder"
msgstr ""

#: literals.py:57 models.py:770
msgid "Watch folder"
msgstr ""

#: literals.py:58
msgid "POP3 email"
msgstr ""

#: literals.py:59 models.py:714 models.py:715
msgid "IMAP email"
msgstr ""

#: models.py:61
msgid "Label"
msgstr "Labela"

#: models.py:62 views.py:556
msgid "Enabled"
msgstr ""

#: models.py:68 models.py:791
msgid "Source"
msgstr ""

#: models.py:170
msgid "Interactive source"
msgstr ""

#: models.py:171
msgid "Interactive sources"
msgstr ""

#: models.py:181
msgid "Device name as returned by the SANE backend."
msgstr ""

#: models.py:182
msgid "Device name"
msgstr ""

#: models.py:187
msgid ""
"Selects the scan mode (e.g., lineart, monochrome, or color). If this option "
"is not supported by your scanner, leave it blank."
msgstr ""

#: models.py:189
msgid "Mode"
msgstr ""

#: models.py:193
msgid ""
"Sets the resolution of the scanned image in DPI (dots per inch). Typical "
"value is 200. If this option is not supported by your scanner, leave it "
"blank."
msgstr ""

#: models.py:196
msgid "Resolution"
msgstr ""

#: models.py:201
msgid ""
"Selects the scan source (such as a document-feeder). If this option is not "
"supported by your scanner, leave it blank."
msgstr ""

#: models.py:204
msgid "Paper source"
msgstr ""

#: models.py:209
msgid ""
"Selects the document feeder mode (simplex/duplex). If this option is not "
"supported by your scanner, leave it blank."
msgstr ""

#: models.py:211
msgid "ADF mode"
msgstr ""

#: models.py:217
msgid "SANE Scanner"
msgstr ""

#: models.py:218
msgid "SANE Scanners"
msgstr ""

#: models.py:265
#, python-format
msgid "Error while scanning; %s"
msgstr ""

#: models.py:297 models.py:763
msgid "Server side filesystem path."
msgstr "Serverska putanja na filesystem-u"

#: models.py:298 models.py:764
msgid "Folder path"
msgstr ""

#: models.py:301
msgid "Width value to be passed to the converter backend."
msgstr "Vrijednost za širinu za konvertovanje na backend-u."

#: models.py:302
msgid "Preview width"
msgstr ""

#: models.py:306
msgid "Height value to be passed to the converter backend."
msgstr "Vrijednost za visinu za konvertovanje na backend-u."

#: models.py:307
msgid "Preview height"
msgstr ""

#: models.py:311 models.py:382
msgid "Whether to expand or not compressed archives."
msgstr "Otpakuj ili ne kompresovane datoteke"

#: models.py:312 models.py:383 models.py:423
msgid "Uncompress"
msgstr ""

#: models.py:317
msgid "Delete the file after is has been successfully uploaded."
msgstr "Obriši datoteku nakon što bude uspješno upload-ana."

#: models.py:319
msgid "Delete after upload"
msgstr ""

#: models.py:326
msgid "Staging folders"
msgstr ""

#: models.py:338
#, python-format
msgid "Error deleting staging file; %s"
msgstr ""

#: models.py:354
#, python-format
msgid "Unable get list of staging files: %s"
msgstr "Onemogući dobivanje liste osnovnih datoteka: %s"

#: models.py:390
msgid "Web forms"
msgstr ""

#: models.py:403 models.py:404
msgid "Out of process"
msgstr ""

#: models.py:410
msgid "Interval in seconds between checks for new documents."
msgstr ""

#: models.py:411
msgid "Interval"
msgstr ""

#: models.py:416
msgid "Assign a document type to documents uploaded from this source."
msgstr ""

#: models.py:418
msgid "Document type"
msgstr "Tip dokumenta"

#: models.py:422
msgid "Whether to expand or not, compressed archives."
msgstr ""

#: models.py:429
msgid "Interval source"
msgstr ""

#: models.py:430
msgid "Interval sources"
msgstr ""

#: models.py:488
msgid "Host"
msgstr ""

#: models.py:489
msgid "SSL"
msgstr ""

#: models.py:491
msgid ""
"Typical choices are 110 for POP3, 995 for POP3 over SSL, 143 for IMAP, 993 "
"for IMAP over SSL."
msgstr ""

#: models.py:492
msgid "Port"
msgstr ""

#: models.py:494
msgid "Username"
msgstr ""

#: models.py:495
msgid "Password"
msgstr ""

#: models.py:499
msgid ""
"Name of the attachment that will contains the metadata type names and value "
"pairs to be assigned to the rest of the downloaded attachments. Note: This "
"attachment has to be the first attachment."
msgstr ""

#: models.py:503
msgid "Metadata attachment name"
msgstr ""

#: models.py:507
msgid ""
"Select a metadata type valid for the document type selected in which to "
"store the email's subject."
msgstr ""

#: models.py:510
msgid "Subject metadata type"
msgstr ""

#: models.py:514
msgid ""
"Select a metadata type valid for the document type selected in which to "
"store the email's \"from\" value."
msgstr ""

#: models.py:517
msgid "From metadata type"
msgstr ""

#: models.py:521
msgid "Store the body of the email as a text document."
msgstr ""

#: models.py:522
msgid "Store email body"
msgstr ""

#: models.py:528
msgid "Email source"
msgstr ""

#: models.py:529
msgid "Email sources"
msgstr ""

#: models.py:537
#, python-format
msgid ""
"Subject metadata type \"%(metadata_type)s\" is not valid for the document "
"type: %(document_type)s"
msgstr ""

#: models.py:551
#, python-format
msgid ""
"\"From\" metadata type \"%(metadata_type)s\" is not valid for the document "
"type: %(document_type)s"
msgstr ""

#: models.py:607
#, python-format
msgid "attachment-%i"
msgstr ""

#: models.py:659
msgid "Timeout"
msgstr ""

#: models.py:665 models.py:666
msgid "POP email"
msgstr ""

#: models.py:707
msgid "IMAP Mailbox from which to check for messages."
msgstr ""

#: models.py:708
msgid "Mailbox"
msgstr ""

#: models.py:771
msgid "Watch folders"
msgstr ""

#: models.py:803
msgid "Log entry"
msgstr ""

#: models.py:804
msgid "Log entries"
msgstr ""

#: permissions.py:7
msgid "Sources setup"
msgstr "Setup izvora"

#: permissions.py:9
msgid "Create new document sources"
msgstr "Kreiraj nove izvore dokumenta"

#: permissions.py:12
msgid "Delete document sources"
msgstr "Obriši izvore dokumenta"

#: permissions.py:15
msgid "Edit document sources"
msgstr "Izmijeni izvore dokumenta"

#: permissions.py:18
msgid "View existing document sources"
msgstr "Pregledaj postojeće izvore dokumenata"

#: permissions.py:21
msgid "Delete staging files"
msgstr ""

#: queues.py:11
msgid "Sources periodic"
msgstr ""

#: queues.py:16
msgid "Check interval source"
msgstr ""

#: queues.py:21
msgid "Handle upload"
msgstr ""

#: queues.py:25
msgid "Upload document"
msgstr ""

#: settings.py:12
msgid "File path to the scanimage program used to control image scanners."
msgstr ""

#: tasks.py:32
#, python-format
msgid "Error processing source: %s"
msgstr ""

#: views.py:51
#, python-format
msgid "Log entries for source: %s"
msgstr ""

#: views.py:107 wizards.py:134
msgid ""
"No interactive document sources have been defined or none have been enabled, "
"create one before proceeding."
msgstr ""

#: views.py:133 views.py:151 views.py:161
msgid "Document properties"
msgstr ""

#: views.py:141
msgid "Files in staging path"
msgstr ""

#: views.py:152
msgid "Scan"
msgstr ""

#: views.py:264
msgid "New document queued for uploaded and will be available shortly."
msgstr ""

#: views.py:315
#, python-format
msgid "Upload a local document from source: %s"
msgstr ""

#: views.py:347
#, python-format
msgid "Document \"%s\" is blocked from uploading new versions."
msgstr ""

#: views.py:399
msgid "New document version queued for uploaded and will be available shortly."
msgstr ""

#: views.py:438
#, python-format
msgid "Upload a new version from source: %s"
msgstr ""

#: views.py:478
#, python-format
msgid "Trigger check for source \"%s\"?"
msgstr ""

#: views.py:491
msgid "Source check queued."
msgstr ""

#: views.py:505
#, python-format
msgid "Create new source of type: %s"
msgstr "Kreiraj novi tip izvora: %s"

#: views.py:525
#, python-format
msgid "Delete the source: %s?"
msgstr ""

#: views.py:544
#, python-format
msgid "Edit source: %s"
msgstr ""

#: views.py:552
msgid "Type"
msgstr ""

#: wizards.py:86
#, fuzzy
#| msgid "Document type"
msgid "Select document type"
msgstr "Tip dokumenta"

#: wizards.py:153
#, python-format
msgid "Step %(step)d of %(total_steps)d: %(step_label)s"
msgstr ""

#: wizards.py:158
msgid "Next step"
msgstr "Sljedeći korak"

#: wizards.py:160
msgid "Document upload wizard"
msgstr ""
